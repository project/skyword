<?php

/**
 * @file
 * Contains skyword.module.
 */

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function skyword_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the skyword module.
    case 'help.page.skyword':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('REST API for Skyword') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_entity_view_alter().
 */
function skyword_entity_view_alter(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display) {
  if (empty($entity)) {
    return;
  }

  $skywordPost = Drupal::entityTypeManager()
                       ->getStorage('skyword_post')
                       ->loadByProperties(['node_ref' => $entity->id()]);

  if (empty($skywordPost)) {
    return;
  }

  $trackingTagValue = reset($skywordPost)->trackingTag->value;
  $trackingTagSrc   = explode("'", $trackingTagValue)[3];

  $trackingTag = [
    '#tag'        => 'script',
    '#attributes' => [
      'async' => '',
      'type'  => 'text/javascript',
      'src'   => $trackingTagSrc,
    ],
  ];

  $build['#attached']['html_head'][] = [$trackingTag, 'skywordTrackingTag'];
}

// @todo: If POSTing a new term, check for duplicates with the same value and parent, and 422 if any are found.
// @todo: Add gravatar support or fetch and save icon set by POST authors.
// @todo: Make field definitions for author->first_name, last_name, byline configurable.
